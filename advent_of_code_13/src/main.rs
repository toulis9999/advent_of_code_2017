use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashMap;

#[derive(Debug, Clone)]
struct Program {
    name: String,
    weight: i64,
    parent: String,
    children: Vec<String>,
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut disc_map: HashMap<String, Program> = HashMap::new();
    for line in f.lines() {
        let l = line.unwrap();
        let line_description: Vec<&str> = l.split(' ').collect();
        let x: &[_] = &['(', ')'];
        let mut record = Program {
            name: line_description[0].to_string(),
            weight: line_description[1].trim_matches(x).parse().unwrap(),
            parent: "".to_string(),
            children: vec![],
        };
        record.children = line_description
            .into_iter()
            .skip(3)
            .map(|x| x.trim_matches(',').to_string())
            .collect();
        disc_map.insert(record.name.clone(), record);
    }

    let mut cloned_map: HashMap<String, Program> = disc_map.clone();
    for entry in disc_map {
        for child in entry.1.children {
            let x = entry.0.clone();
            match cloned_map.get_mut(&child) {
                Some(v) => {
                    v.parent = x;
                }
                None => {}
            };
        }
    }
    for entry in cloned_map {
        if entry.1.parent == "" {
            println!("{:?}", entry);
        }
    }
}
