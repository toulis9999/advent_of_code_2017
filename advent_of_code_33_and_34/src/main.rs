fn main() {
    let step = 301;

    //problem 1
    let mut entries: Vec<u32> = vec![0];
    entries.reserve_exact(2018);
    let mut idx: usize = 0;
    for i in 1..2018 {
        idx = ((idx + step) % entries.len()) + 1;
        entries.insert(idx, i);
    }
    let p = entries.iter().position(|x| *x == 2017).unwrap();
    println!("{}", entries[p + 1]);

    //problem 2
    let mut entries_len = 1;
    let mut idx: usize = 0;
    let mut output = 0;
    for i in 1..50000000 {
        idx = ((idx + step) % entries_len) + 1;
        if idx == 1 {
            output = i;
        }
        entries_len += 1;
    }
    println!("{}", output);
}
