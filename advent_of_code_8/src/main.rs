use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashSet;

fn main() {
    let mut sum: u64 = 0;
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    for line in f.lines() {
        let l = line.unwrap();
        let unsorted_vec: Vec<&str> = l.split(' ').collect();
        let mut vec: Vec<String> = vec![];
        for phrase in unsorted_vec {
            let mut chars: Vec<char> = phrase.chars().collect();
            chars.sort_by(|a, b| b.cmp(a));
            let phrase_sorted: String = chars.into_iter().collect();
            vec.push(phrase_sorted);
        }
        let vec_len = vec.len();
        let set: HashSet<String> = vec.into_iter().collect();
        if vec_len == set.len() {
            sum += 1;
        }
    }
    println!("{}", sum);
}
