use std::collections::HashMap;

fn decrease_idx(idx: i64) -> i64 {
    idx - 1
}

fn increase_idx(idx: i64) -> i64 {
    idx + 1
}

fn write_zero(idx: i64, tape: &mut HashMap<i64, bool>) {
    tape.insert(idx, false);
}

fn write_one(idx: i64, tape: &mut HashMap<i64, bool>) {
    tape.insert(idx, true);
}

fn advance(idx: i64, tape: &mut HashMap<i64, bool>, sp: &StateProcessor) -> (i64, char) {
    let cur_val = *tape.entry(idx).or_insert(false);
    let mut ret: (i64, char) = (0, ' ');
    if cur_val {
        (sp.change_current_val_on_one)(idx, tape);
        ret.0 = (sp.change_index_on_one)(idx);
        ret.1 = sp.state_to_continue_on_one;
    } else {
        (sp.change_current_val_on_zero)(idx, tape);
        ret.0 = (sp.change_index_on_zero)(idx);
        ret.1 = sp.state_to_continue_on_zero;
    }
    ret
}

struct StateProcessor {
    change_current_val_on_zero: fn(i64, &mut HashMap<i64, bool>),
    change_index_on_zero: fn(i64) -> i64,
    state_to_continue_on_zero: char,
    change_current_val_on_one: fn(i64, &mut HashMap<i64, bool>),
    change_index_on_one: fn(i64) -> i64,
    state_to_continue_on_one: char,
}

const STATE_A: StateProcessor = StateProcessor {
    change_current_val_on_zero: write_one,
    change_index_on_zero: increase_idx,
    state_to_continue_on_zero: 'B',
    change_current_val_on_one: write_zero,
    change_index_on_one: decrease_idx,
    state_to_continue_on_one: 'F',
};

const STATE_B: StateProcessor = StateProcessor {
    change_current_val_on_zero: write_zero,
    change_index_on_zero: increase_idx,
    state_to_continue_on_zero: 'C',
    change_current_val_on_one: write_zero,
    change_index_on_one: increase_idx,
    state_to_continue_on_one: 'D',
};

const STATE_C: StateProcessor = StateProcessor {
    change_current_val_on_zero: write_one,
    change_index_on_zero: decrease_idx,
    state_to_continue_on_zero: 'D',
    change_current_val_on_one: write_one,
    change_index_on_one: increase_idx,
    state_to_continue_on_one: 'E',
};

const STATE_D: StateProcessor = StateProcessor {
    change_current_val_on_zero: write_zero,
    change_index_on_zero: decrease_idx,
    state_to_continue_on_zero: 'E',
    change_current_val_on_one: write_zero,
    change_index_on_one: decrease_idx,
    state_to_continue_on_one: 'D',
};

const STATE_E: StateProcessor = StateProcessor {
    change_current_val_on_zero: write_zero,
    change_index_on_zero: increase_idx,
    state_to_continue_on_zero: 'A',
    change_current_val_on_one: write_one,
    change_index_on_one: increase_idx,
    state_to_continue_on_one: 'C',
};

const STATE_F: StateProcessor = StateProcessor {
    change_current_val_on_zero: write_one,
    change_index_on_zero: decrease_idx,
    state_to_continue_on_zero: 'A',
    change_current_val_on_one: write_one,
    change_index_on_one: increase_idx,
    state_to_continue_on_one: 'A',
};

fn main() {
    let mut rules: HashMap<char, StateProcessor> = HashMap::new();
    rules.insert('A', STATE_A);
    rules.insert('B', STATE_B);
    rules.insert('C', STATE_C);
    rules.insert('D', STATE_D);
    rules.insert('E', STATE_E);
    rules.insert('F', STATE_F);

    let mut tape: HashMap<i64, bool> = HashMap::new();
    tape.insert(0, false);
    let mut current_index = 0;
    let mut current_state = 'A';

    for _ in 0..12994925 {
        let ret = advance(current_index, &mut tape, &rules[&current_state]);
        current_index = ret.0;
        current_state = ret.1;
    }

    let mut sum = 0;
    for entry in tape {
        if entry.1 {
            sum += 1;
        }
    }
    println!("{}", sum);
}
