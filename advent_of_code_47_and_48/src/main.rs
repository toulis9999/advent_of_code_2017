use std::fs::File;
use std::io::BufReader;
use std::io::prelude::BufRead;

fn get_indices_with_ports(comps: &Vec<(u32, u32)>, n: u32) -> Vec<usize> {
    let mut ret: Vec<usize> = Vec::new();
    for l in comps.iter().enumerate() {
        if (l.1).0 == n || (l.1).1 == n {
            ret.push(l.0);
        }
    }
    ret
}

fn get_secondary_ports(comp: &(u32, u32), primary_port: u32) -> u32 {
    if comp.0 == primary_port {
        comp.1
    } else {
        comp.0
    }
}

fn calculate_bridge_strength(bridge: &Vec<(u32, u32)>) -> u32 {
    let mut sum = 0;
    for comp in bridge {
        sum += comp.0 + comp.1;
    }
    sum
}

fn iterate(
    prev_port: u32,
    bridge: &Vec<(u32, u32)>,
    comps: &Vec<(u32, u32)>,
    out: &mut Vec<(u32, u32)>,
) {
    let idxs = get_indices_with_ports(comps, prev_port);
    if idxs.len() == 0 {
        out.push((bridge.len() as u32, calculate_bridge_strength(bridge)));
    }
    for idx in idxs {
        let mut cloned_comps = comps.clone();
        let mut cloned_bridge = bridge.clone();
        let comp = cloned_comps.remove(idx);
        let last_port = get_secondary_ports(&comp, prev_port);
        cloned_bridge.push(comp);
        iterate(last_port, &cloned_bridge, &cloned_comps, out);
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut components: Vec<(u32, u32)> = Vec::new();

    for line in f.lines() {
        let nums: Vec<u32> = line.unwrap()
            .split('/')
            .map(|x| x.parse().unwrap())
            .collect();
        let component: (u32, u32) = (nums[0], nums[1]);
        components.push(component);
    }

    let mut out: Vec<(u32, u32)> = vec![];
    iterate(0, &vec![], &components, &mut out);

    //problem 1
    out.sort_by(|a, b| {
        let x = a.1;
        let y = b.1;
        y.cmp(&x)
    });
    println!("{}", out[0].1);

    //problem 2
    iterate(0, &vec![], &components, &mut out);
    out.sort_by(|a, b| {
        let x = a.0;
        let y = b.0;
        y.cmp(&x)
    });
    let mut val = out[0];
    let mut max_str = out[0].1;
    for t in out {
        if t.0 < val.0 {
            break; //bail early since already sorted by bridge len
        } else {
            if t.1 > val.1 {
                max_str = t.1;
                val = t;
            }
        }
    }
    println!("{}", max_str);
}
