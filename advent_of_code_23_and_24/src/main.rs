use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashMap;
use std::collections::HashSet;

fn nodes_connected_to_node_with_val(
    connections_map: &HashMap<u64, Vec<u64>>,
    val: u64,
) -> HashSet<u64> {
    let mut connected_nodes: HashSet<u64> = HashSet::new();
    let mut starting_set = connected_nodes.clone();
    connected_nodes.insert(val);

    let mut diff: HashSet<u64> = connected_nodes
        .difference(&starting_set)
        .map(|x| *x)
        .collect();

    while diff.len() > 0 {
        starting_set = connected_nodes.clone();
        for val in &diff {
            let connections = &connections_map[&val];
            for connection in connections {
                connected_nodes.insert(*connection);
            }
        }
        diff = connected_nodes
            .difference(&starting_set)
            .map(|x| *x)
            .collect();
    }

    connected_nodes
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut connections_map: HashMap<u64, Vec<u64>> = HashMap::new();
    for line in f.lines() {
        let l = line.unwrap();
        let line_description: Vec<&str> = l.split(' ').collect();
        let key = line_description[0].parse::<u64>().unwrap();
        let vals: Vec<u64> = line_description
            .into_iter()
            .skip(2)
            .map(|x| x.trim_right_matches(',').parse::<u64>().unwrap())
            .collect();
        connections_map.insert(key, vals);
    }

    //problem 1
    let x = nodes_connected_to_node_with_val(&connections_map, 0);
    println!("{}", x.len());

    //problem 2
    let mut num_groups = 0;
    while connections_map.len() > 0 {
        let opt = match connections_map.keys().nth(0) {
            Some(val) => *val,
            None => panic!(""),
        };
        let x = nodes_connected_to_node_with_val(&connections_map, opt);
        for val in x {
            connections_map.remove(&val);
        }
        num_groups += 1;
    }
    println!("{}", num_groups);
}
