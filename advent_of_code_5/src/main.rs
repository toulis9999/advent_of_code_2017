enum Direction {
    Bottom = 1,
    Left = 3,
    Top = 5,
    Right = 7,
}

fn find_outter(n: f64) -> i64 {
    let mut tmp = n.sqrt().ceil() as i64;
    if tmp % 2 == 0 {
        tmp = tmp + 1;
    }
    ((tmp - 3) / 2) + 1
}

fn find_bound(n: i64, d: Direction) -> i64 {
    let tmp = 2 * (n + 1) - 1;
    tmp * tmp - n * d as i64
}

fn main() {
    let num = 277678;
    let outter = find_outter(num as f64);
    let mut distances = [
        (find_bound(outter, Direction::Bottom) - num).abs(),
        (find_bound(outter, Direction::Left) - num).abs(),
        (find_bound(outter, Direction::Top) - num).abs(),
        (find_bound(outter, Direction::Right) - num).abs(),
    ];
    distances.sort();
    println!("{}", distances[0] + outter);
}
