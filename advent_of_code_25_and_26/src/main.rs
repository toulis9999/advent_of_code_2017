use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn get_cost(vec: &Vec<(u64, u64)>, n: u64) -> (u64, bool) {
    let mut total = 0;
    let mut caught = false;
    for (i, entry) in vec.iter().enumerate() {
        if ((entry.0 + entry.1 + n) % entry.1) == 0 {
            total += entry.0 * (entry.1 + 2) / 2;
            if i == 0 {
                caught = true
            };
        };
    }
    (total, caught)
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut entries: Vec<(u64, u64)> = vec![];
    for line in f.lines() {
        let mut l = line.unwrap();
        l = l.replace(':', "");
        let x: Vec<&str> = l.split_whitespace().collect();
        let position = x[0].parse::<u64>().unwrap();
        let length = x[1].parse::<u64>().unwrap();
        entries.push((position, length * 2 - 2));
    }

    //problem 1
    println!("{}", get_cost(&entries, 0).0);

    //problem 2
    let mut picos_to_wait: u64 = 0;
    loop {
        let total = get_cost(&entries, picos_to_wait);
        if total.0 == 0 && total.1 == false {
            break;
        }
        picos_to_wait += 1;
    }
    println!("{}", picos_to_wait);
}
