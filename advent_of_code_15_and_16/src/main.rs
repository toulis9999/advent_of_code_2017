use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use std::collections::HashMap;

fn increment_op(lhs: i64, rhs: i64) -> i64 {
    lhs + rhs
}
fn decrement_op(lhs: i64, rhs: i64) -> i64 {
    lhs - rhs
}
fn less_than_op(lhs: i64, rhs: i64) -> bool {
    lhs < rhs
}
fn greater_than_op(lhs: i64, rhs: i64) -> bool {
    lhs > rhs
}
fn equal_op(lhs: i64, rhs: i64) -> bool {
    lhs == rhs
}
fn not_equal_op(lhs: i64, rhs: i64) -> bool {
    lhs != rhs
}
fn less_than_or_eual_op(lhs: i64, rhs: i64) -> bool {
    lhs <= rhs
}
fn greater_than_or_equal_op(lhs: i64, rhs: i64) -> bool {
    lhs >= rhs
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut register_map: HashMap<String, i64> = HashMap::new();
    let mut max_register_val = std::i64::MIN;

    for line in f.lines() {
        let l = line.unwrap();
        let line_description: Vec<&str> = l.split(' ').collect();

        let modify_operation = match line_description[1].as_ref() {
            "dec" => decrement_op,
            &_ => increment_op,
        };

        let other_register_value = *register_map
            .entry(line_description[4].to_string())
            .or_insert(0);

        let register_modify_value = register_map
            .entry(line_description[0].to_string())
            .or_insert(0);

        let comparison_operation = match line_description[5].as_ref() {
            "==" => equal_op,
            "!=" => not_equal_op,
            "<" => less_than_op,
            ">" => greater_than_op,
            "<=" => less_than_or_eual_op,
            &_ => greater_than_or_equal_op,
        };

        let comparison_rhs = line_description[6].parse::<i64>().unwrap();
        let modify_op_rhs = line_description[2].parse::<i64>().unwrap();

        let reg_val = *register_modify_value;
        let val_to_insert = if comparison_operation(other_register_value, comparison_rhs) {
            modify_operation(reg_val, modify_op_rhs)
        } else {
            reg_val
        };
        if max_register_val < val_to_insert {
            max_register_val = val_to_insert;
        }
        *register_modify_value = val_to_insert;
    }

    //problem 1
    let mut v: Vec<i64> = register_map.values().map(|x| *x).collect();
    v.sort();
    println!("{:?}", v[v.len() - 1]);

    //problem 2
    println!("{}", max_register_val);
}
