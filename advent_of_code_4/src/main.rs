use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let f = BufReader::new(File::open("input.txt").unwrap());
    let mut sum = 0;
    for line in f.lines() {
        let l = line.unwrap();
        let vec: Vec<i32> = l.split_whitespace()
            .map(|x| x.parse::<i32>().unwrap())
            .collect();
        let mut res: Vec<i32> = vec![];
        for i in 0..vec.len() {
            let v_copy = vec.clone();
            let tmp: Vec<i32> = v_copy
                .into_iter()
                .filter(|x| *x as f32 % vec[i] as f32 == 0.0)
                .collect();
            if tmp.len() > 1 {
                res = tmp;
            }
        }
        res.sort();
        sum += res[1] / res[0];
    }
    println!("{}", sum);
}
