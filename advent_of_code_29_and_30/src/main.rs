fn main() {
    const GEN1_FACTOR: u64 = 16807;
    const GEN2_FACTOR: u64 = 48271;
    const MOD_REM: u64 = 2147483647;
    const MASK: u64 = (1 << 16) - 1;

    let mut gen1_starting_val: u64 = 783;
    let mut gen2_starting_val: u64 = 325;

    //problem 1
    let mut count = 0;
    for _ in 0..40000000 {
        let v1 = (gen1_starting_val * GEN1_FACTOR) % MOD_REM;
        let v2 = (gen2_starting_val * GEN2_FACTOR) % MOD_REM;

        if (v1 & MASK) == (v2 & MASK) {
            count += 1;
        }
        gen1_starting_val = v1;
        gen2_starting_val = v2;
    }
    println!("{}", count);

    //problem 2
    gen1_starting_val = 783;
    gen2_starting_val = 325;
    let mut count = 0;
    let mut v1: u64;
    let mut v2: u64;
    for _ in 0..5000000 {
        loop {
            v1 = (gen1_starting_val * GEN1_FACTOR) % MOD_REM;
            gen1_starting_val = v1;
            if v1 % 4 == 0 {
                break;
            }
        }
        loop {
            v2 = (gen2_starting_val * GEN2_FACTOR) % MOD_REM;
            gen2_starting_val = v2;
            if v2 % 8 == 0 {
                break;
            }
        }

        if (v1 & MASK) == (v2 & MASK) {
            count += 1;
        }
    }
    println!("{}", count);
}
