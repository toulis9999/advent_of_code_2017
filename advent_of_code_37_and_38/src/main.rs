use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug)]
struct TraversingEntity {
    pos: (i32, i32),
    dir: (i32, i32),
    steps: usize,
}

impl TraversingEntity {
    fn advance(&mut self) {
        self.pos.0 = self.pos.0 + self.dir.0;
        self.pos.1 = self.pos.1 + self.dir.1;
        self.steps += 1;
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut path : Vec<Vec<char>> = Vec::new();
    for line in f.lines() {
        let c : Vec<char> = line.unwrap().as_bytes().to_vec().iter().map(|&x| x as char).collect();
        path.push(c);
    }

    let mut entity = TraversingEntity{ pos: (path[0].iter().position(|&x|  x == '|').unwrap() as i32, 0),
        dir: (0, 1), steps: 0 };
    
    let mut next_char = path[(entity.pos.1 + entity.dir.1) as usize][(entity.pos.0 + entity.dir.0) as usize];
    let mut letters = String::new();
    loop
    {
        match next_char {
            '+' => match entity.dir {
                (0, 1) | (0, -1) => {
                    entity.advance();
                    let left_char = path[entity.pos.1 as usize][(entity.pos.0 - 1) as usize];
                    let right_char = path[entity.pos.1 as usize][(entity.pos.0 + 1) as usize];
                    if left_char != ' ' {entity.dir = (-1, 0); }
                    if right_char != ' ' {entity.dir = (1, 0); }
                },
                (-1, 0) | (1, 0) => {
                    entity.advance();
                    let up_char = path[(entity.pos.1 - 1) as usize][entity.pos.0 as usize];
                    let down_char = path[(entity.pos.1 + 1) as usize][entity.pos.0 as usize];
                    if up_char != ' ' {entity.dir = (0, -1); }
                    if down_char != ' ' {entity.dir = (0, 1); }
                },
                _ => panic!("Error, unexpected direction"),
            },
            x @ 'A'...'Z' => {
                letters.push(x);
                entity.advance();
            }
            '|' | '-' => entity.advance(),
            _ => break,
        }
        next_char = path[(entity.pos.1 + entity.dir.1) as usize][(entity.pos.0 + entity.dir.0) as usize];
    }
    //problem 1
    println!("{}", letters);
    //problem 2
    println!("{}", entity.steps + 1);
}
