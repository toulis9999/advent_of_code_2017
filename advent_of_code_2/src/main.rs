use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let mut sum: u64 = 0;
    let fi = File::open("input.txt").expect("file not found");
    let mut f = BufReader::new(fi);
    let mut buf: Vec<u8> = Vec::new();
    f.read_to_end(buf.as_mut()).expect("could not read bufer");

    let buf_length = buf.len();

    for i in 0..buf_length {
        let index_to_compare = (i + (buf_length / 2)) % buf_length;
        if buf[i] == buf[index_to_compare] {
            let tmp = buf[i] as u64;
            sum += tmp - 48;
        }
    }
    println!("{}", sum);
}
