use std::fs::File;
use std::io::BufReader;
use std::io::prelude::BufRead;

use std::collections::HashMap;

#[derive(Debug, Clone)]
enum Op {
    SetOp,
    SubOp,
    MulOp,
    JnzOp,
}

#[derive(Debug, Clone)]
enum Operand {
    Value(i64),
    Register(char),
}

#[derive(Debug, Clone)]
struct Instruction {
    operation: Op,
    left_operand: Operand,
    right_operand: Operand,
}

#[derive(Debug)]
struct ProgramState {
    map: HashMap<char, i64>,
    instructions: Vec<Instruction>,
    instruction_index: i64,
    finished: bool,
    num_mul_op_called : u64,
}

impl ProgramState {
    fn new() -> ProgramState {
        ProgramState {
            map: HashMap::new(),
            instructions: vec![],
            instruction_index: 0,
            finished: false,
            num_mul_op_called: 0,
        }
    }

    fn process_instruction(&mut self) {
        let q = &self.instructions[self.instruction_index as usize];
        let operand_val = match q.right_operand {
            Operand::Register(r) => *self.map.entry(r).or_insert(0),
            Operand::Value(v) => v,
        };

        let l_operand_val = match q.left_operand {
            Operand::Register(r) => *self.map.entry(r).or_insert(0),
            Operand::Value(v) => v,
        };

        let l_operand_reg_name = match q.left_operand {
            Operand::Register(r) => Some(r),
            Operand::Value(_) => None,
        };

        match &q.operation {
            &Op::SetOp => {self.map.insert(l_operand_reg_name.unwrap(), operand_val);}
            &Op::SubOp => {self.map.insert(l_operand_reg_name.unwrap(), l_operand_val - operand_val);}
            &Op::MulOp => {self.map.insert(l_operand_reg_name.unwrap(), l_operand_val * operand_val);
                self.num_mul_op_called += 1;
            }
            &Op::JnzOp => {self.instruction_index += if l_operand_val != 0 { operand_val - 1 } else { 0 };}
        }
        self.instruction_index += 1;
        if self.instruction_index < 0 || self.instruction_index >= self.instructions.len() as i64 {
            self.finished = true;
        }
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);

    let mut state0: ProgramState = ProgramState::new();

    for line in f.lines() {
        let l = line.unwrap();
        let line_description: Vec<&str> = l.split(' ').collect();
        let instruction_op = match line_description[0] {
            "set" => Op::SetOp,
            "sub" => Op::SubOp,
            "mul" => Op::MulOp,
            "jnz" => Op::JnzOp,
            _ => panic!("Unexpected Instruction!"),
        };

        let l_operand: Operand = match line_description[1].chars().nth(0).unwrap() {
            'a'...'z' => Operand::Register(line_description[1].chars().nth(0).unwrap()),
            '0'...'9' | '-' => Operand::Value(line_description[1].parse::<i64>().unwrap()),
            _ => panic!("Unexpected Operand!"),
        };

        let r_operand: Operand = match line_description[2].chars().nth(0).unwrap() {
            'a'...'z' =>  Operand::Register(line_description[2].chars().nth(0).unwrap()),
            '0'...'9' | '-' => Operand::Value(line_description[2].parse::<i64>().unwrap()),
            _ => panic!("Unexpected Operand!"),
        };

        let instruction = Instruction {
            operation: instruction_op,
            left_operand: l_operand,
            right_operand: r_operand,
        };

        state0.instructions.push(instruction.clone());
    }

    //problem 1
    while !state0.finished {
        state0.process_instruction();
    }
    println!("{}", state0.num_mul_op_called);

    //problem 2
    // after tranforming the original instructions and simplifying the code we get this Rust equivalent:
    // let mut b = 108400;
    // let c = 125400;
    // let mut h = 0;
    // loop {
    //     let mut f = 1;
    //     for d in 2..b {
    //         for e in 2..b {
    //             if (d * e) == b {
    //                 f = 0;
    //             }
    //         }
    //     }
    //     if f == 0 {
    //         h += 1;
    //     }
    //     if b == c { break; }
    //     b += 17;
    // }

    //after optimising we get:
    let mut b = 108400;
    let c = 125400;
    let mut h = 0;
    loop {
        'outer: for d in 2..b {
            for e in (b/d)..b {
                if (d * e) == b {
                    h += 1;
                    break 'outer;
                }
                if (d * e) > b {
                    break;
                }
            }
        }
        if b >= c { break; }
        b += 17;
    }
    println!("{}", h)
}
