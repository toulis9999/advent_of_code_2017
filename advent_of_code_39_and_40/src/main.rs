use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Debug, PartialEq, Eq, Clone)]
struct Vec3 {
    x: i32,
    y: i32,
    z: i32,
}

#[derive(Debug, Clone)]
struct Particle {
    pos: Vec3,
    vel: Vec3,
    acc: Vec3,
    dist: i32,
}

impl PartialEq for Particle {
    fn eq(&self, other: &Particle) -> bool {
        self.pos == other.pos
    }
}
impl Eq for Particle {}

impl Particle {
    fn new(p: Vec3, v: Vec3, a: Vec3) -> Particle {
        let mut p = Particle {pos: p, vel: v, acc: a, dist: 0};
        p.dist = p.pos.manhattan_dist();
        p
    }
    fn advance(&mut self) {
        self.vel.add(&self.acc);
        self.pos.add(&self.vel);
        self.dist = self.pos.manhattan_dist();
    }
}

impl Vec3 {
    fn add(&mut self, other: &Vec3) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }
    fn manhattan_dist(&self) -> i32 {
        self.x.abs() + self.y.abs() + self.z.abs()
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    let mut particles: Vec<Particle> = Vec::new();
    let mut particles2: Vec<Particle> = Vec::new();

    for line in f.lines().enumerate() {
        let x = line.1.unwrap();
        let y: Vec<&str> = x.split(',').collect();
        let part = Particle::new(
            Vec3{x:y[0].parse::<i32>().unwrap(), y:y[1].parse::<i32>().unwrap(), z:y[2].parse::<i32>().unwrap()},
            Vec3{x:y[3].parse::<i32>().unwrap(), y:y[4].parse::<i32>().unwrap(), z:y[5].parse::<i32>().unwrap()},
            Vec3{x:y[6].parse::<i32>().unwrap(), y:y[7].parse::<i32>().unwrap(), z:y[8].parse::<i32>().unwrap()},
        );
        particles.push(part.clone());
        particles2.push(part);
    }

    //problem 1
    let mut min_pos = -1;
    for _ in 0..1000 {
        {
            let min = particles.iter().min_by(|x,y| x.dist.cmp(&y.dist)).unwrap();
            min_pos = particles.iter().position(|x| x.dist == min.dist).unwrap() as i32;
        }
        for p in &mut particles {
            p.advance();
        }
    }
    println!("{}", min_pos);

    //problem 2
    for _ in 0..50 {
        let cloned_p = particles2.clone();
        for p in &cloned_p {
            let init_len = particles2.len();
            particles2.retain(|x| x != p);
            if particles2.len() == (init_len - 1) {
                particles2.push(p.clone());
            }
        }
        for p in &mut particles2 {
            p.advance();
        }
    }
    println!("{}", particles2.len());
}
