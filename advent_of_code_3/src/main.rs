use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let f = BufReader::new(File::open("input.txt").unwrap());
    let mut sum = 0;
    for line in f.lines() {
        let l = line.unwrap();
        let vec: Vec<_> = l.split_whitespace()
            .map(|x| x.parse::<i32>().unwrap())
            .collect();
        let mut minimum = vec[0];
        let mut maximum = minimum;
        for i in 1..vec.len() {
            let num = vec[i];
            minimum = if num < minimum { num } else { minimum };
            maximum = if num > maximum { num } else { maximum };
        }
        sum += maximum - minimum;
    }
    println!("{}", sum);
}
