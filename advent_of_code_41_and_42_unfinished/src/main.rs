use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashMap;

fn extract_string_from_indices(s: &str, idxs: &Vec<usize>) -> String {
    idxs.iter().map(|x| s.chars().nth(*x).unwrap()).collect()
}

fn activated_pixels(s: &str) -> u64 {
    s.matches('#').count() as u64
}

fn expand_rules(block: &str, r: &mut HashMap<String, String>) {
    let mut res: String = String::new();
    let perms = get_all_permutations(&block);
    for perm in &perms {
        if r.contains_key(perm) {
            res = r[perm].clone();
            break;
        }
    }
    for perm in &perms {
        r.insert(perm.clone(), res.clone());
    }
}

fn combine(s: &Vec<String>) -> String {
    debug_assert!(!s.is_empty());
    let side_len = (s.len() as f64).sqrt() as usize;
    let block_l = (s[0].len() as f64).sqrt() as usize;

    let mut ret = String::with_capacity(s.len() * s[0].len());
    let mut cloned_s = s.clone();
    while !cloned_s.is_empty() {
        for _ in 0..block_l {
            for i in 0..side_len {
                let tmp: String = cloned_s[i].drain(..block_l).collect();
                ret.push_str(&tmp);
            }
        }
        for _ in 0..side_len {
            cloned_s.remove(0);
        }
    }
    ret
}

fn split(s: &str) -> Vec<String> {
    let side_len = (s.len() as f64).sqrt() as usize;
    let mut inputs: Vec<String> = Vec::with_capacity(side_len / 2);
    let mut cloned_s = s.to_string();
    if s.len() == 2 || s.len() == 3 {
        inputs.push(cloned_s);
    } else if s.len() % 2 == 0 {
        while !cloned_s.is_empty() {
            let mut side1: String = cloned_s.drain(..side_len).collect();
            let mut side2: String = cloned_s.drain(..side_len).collect();
            while !side1.is_empty() {
                let s1: String = side1.drain(..2).collect();
                let s2: String = side2.drain(..2).collect();
                let tmp: String = s1 + &s2;
                inputs.push(tmp);
            }
        }
    } else if s.len() % 3 == 0 {
        while !cloned_s.is_empty() {
            let mut side1: String = cloned_s.drain(..side_len).collect();
            let mut side2: String = cloned_s.drain(..side_len).collect();
            let mut side3: String = cloned_s.drain(..side_len).collect();
            while !side1.is_empty() {
                let s1: String = side1.drain(..3).collect();
                let s2: String = side2.drain(..3).collect();
                let s3: String = side3.drain(..3).collect();
                let tmp: String = s1 + &s2 + &s3;
                inputs.push(tmp);
            }
        }
    } else {
        panic!("Unexpected string size when splitting");
    }
    inputs
}

fn rotate(b: &str) -> String {
    match b.len() {
        4 => extract_string_from_indices(b, &vec![2, 0, 3, 1]),
        9 => extract_string_from_indices(b, &vec![6, 3, 0, 7, 4, 1, 8, 5, 2]),
        _ => panic!("Error, unexpected size while rotating"),
    }
}

fn flip(b: &str) -> String {
    match b.len() {
        4 => extract_string_from_indices(b, &vec![2, 3, 0, 1]),
        9 => extract_string_from_indices(b, &vec![6, 7, 8, 3, 4, 5, 0, 1, 2]),
        _ => panic!("Error, unexpected size while rotating"),
    }
}

fn get_all_permutations(b: &str) -> Vec<String> {
    let mut ret: Vec<String> = Vec::with_capacity(8);
    let perm1 = rotate(b);
    let perm2 = rotate(&perm1);
    let perm3 = rotate(&perm2);
    let perm4 = flip(b);
    let perm5 = rotate(&perm4);
    let perm6 = rotate(&perm5);
    let perm7 = rotate(&perm6);
    ret.push(b.to_string());
    ret.push(perm1);
    ret.push(perm2);
    ret.push(perm3);
    ret.push(perm4);
    ret.push(perm5);
    ret.push(perm6);
    ret.push(perm7);
    ret
}

fn advance_state(prev_state: &str, mut rules: &mut HashMap<String, String>, n: u64) -> String {
    let mut ret = prev_state.to_string();
    for _ in 0..n {
        let mut new_state: Vec<String> = vec![];
        let split_state = split(&ret);
        for block in split_state {
            expand_rules(&block, &mut rules);
            if rules.contains_key(&block) {
                for spr in split(&rules[&block]) {
                    new_state.push(spr);
                }
            }
            debug_assert!(!new_state.is_empty());
        }
        ret = combine(&new_state);
    }
    ret
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    let state = ".#...####";
    let mut rules: HashMap<String, String> = f.lines()
        .map(|line| {
            let x = line.unwrap();
            let y: Vec<&str> = x.split(" => ").collect();
            let from: String = y[0].split('/').collect();
            let to: String = y[1].split('/').collect();
            (from, to)
        })
        .collect();

    let mut sum = 0;
    let s = split(&advance_state(state, &mut rules, 3));
    for block in s {
        let s2 = advance_state(&block, &mut rules, 2);
        sum += activated_pixels(&s2);
    }
    println!("{}", sum);
    println!("{}", activated_pixels(&advance_state(state, &mut rules, 5)));
}
