use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashSet;

fn main() {
    let mut sum: u64 = 0;
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    for line in f.lines() {
        let l = line.unwrap();
        let vec: Vec<&str> = l.split(' ').collect();
        let set: HashSet<&str> = l.split(' ').collect();
        if vec.len() == set.len() {
            sum += 1;
        }
    }
    println!("{}", sum);
}
