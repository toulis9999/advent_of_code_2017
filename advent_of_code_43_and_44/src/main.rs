use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::collections::HashMap;

const DIRECTIONS: [(i64,i64); 4] = [(0,-1), (-1,0), (0,1), (1,0),];

struct Carrier {
    pos: (i64,i64),
    modify_node: fn((i64,i64), char, &mut HashMap<(i64,i64), char>) -> bool,
    num_infections: u64,
    dir: i8,
}

fn modify_problem1(cur_pos: (i64,i64), c: char, board: &mut HashMap<(i64,i64), char>) -> bool {
    match c {
        '#' => {board.insert(cur_pos, '.'); false}
        '.' => {board.insert(cur_pos, '#'); true}
        _ => panic!("oops"),
    }
}

fn modify_problem2(cur_pos: (i64,i64), c: char, board: &mut HashMap<(i64,i64), char>) -> bool {
    match c {
        '#' => {board.insert(cur_pos, 'F'); false}
        '.' => {board.insert(cur_pos, 'W'); false}
        'W' => {board.insert(cur_pos, '#'); true}
        'F' => {board.insert(cur_pos, '.'); false}
        _ => panic!("oops"),
    }
}

impl Carrier {
    fn process_dir(&mut self, c: char) {
        match c {
            '#' => {self.turn_right();}
            '.' => {self.turn_left();}
            'W' => {}
            'F' => {
                self.turn_right();
                self.turn_right();
            }
            _ => panic!("oops"),
        }
    }

    fn get_direction(&self) -> (i64,i64) {
        DIRECTIONS[self.dir as usize]
    }

    fn turn_left(&mut self) {
        self.dir += 1;
        self.dir %= DIRECTIONS.len() as i8;
    }

    fn turn_right(&mut self) {
        self.dir -= 1;
        if self.dir == -1 {
            self.dir = DIRECTIONS.len() as i8 - 1;
        }
    }

    fn advance(&mut self, board: &mut HashMap<(i64,i64), char>) {
        let curr_node = self.get_current_node_state(board);
        if (self.modify_node)(self.pos, curr_node, board) {
            self.num_infections += 1;
        }
        self.process_dir(curr_node);
        self.pos.0 += self.get_direction().0;
        self.pos.1 += self.get_direction().1;
    }

    fn get_current_node_state(&self, board: &HashMap<(i64,i64), char>) -> char {
        let mut ret = '.';
        if board.contains_key( &self.pos ) {
            ret = board[&self.pos];
        }
        ret
    }
}

fn main() {
    let fi = File::open("input.txt").expect("file not found");
    let f = BufReader::new(fi);
    let mut board: HashMap<(i64,i64), char> = HashMap::new();
    for (i,line) in f.lines().enumerate() {
         for (j, c) in line.unwrap().chars().enumerate() {
             match c {
                 '#' | '.' => {board.insert((j as i64,i as i64), c);}
                 _ => panic!("Oops unexpected board element '{}' at position {:?}", c, (i,j)),
             }
         }
    }
    
    let mut board2 = board.clone();

    //problem 1
    let mut c = Carrier {
        pos: (12,12),
        modify_node: modify_problem1,
        num_infections: 0,
        dir: 0,
    };
    for _ in 0..10000 {
        c.advance(&mut board);
    }
    println!("{:?}", c.num_infections);

        //problem 1
    let mut c = Carrier {
        pos: (12,12),
        modify_node: modify_problem2,
        num_infections: 0,
        dir: 0,
    };
    for _ in 0..10000000 {
        c.advance(&mut board2);
    }
    println!("{:?}", c.num_infections);

}